# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2018 SerialLab Corp.  All rights reserved.
adjectives = [
    'advancing', 'ancient', 'autumn', 'billowing', 'bitter', 'black', 'blue',
    'bold',
    'broad', 'broken', 'calm', 'cold', 'cool', 'crimson', 'curly', 'deft',
    'dark', 'dawn', 'dim', 'divine', 'dry', 'empty', 'falling', 'fancy',
    'flat', 'floral', 'fragrant', 'frosty', 'grey', 'green', 'hidden',
    'holy',
    'icy', 'intrepid', 'late', 'lost', 'left', 'lively', 'legal', 'opaque',
    'misty', 'morning', 'maroon', 'mute', 'nameless', 'natural', 'open', 'olive',
    'orange', 'patient', 'plain', 'slow', 'solitary', 'punctual', 'quiet',
    'rapid',
    'raining', 'remaining', 'bright', 'staid', 'rectangular', 'royal', 'shining', 'safe',
    'sole', 'silent', 'stone', 'snowy', 'soft', 'solitary', 'sparkling',
    'spring',
    'square', 'steep', 'still', 'summer', 'super', 'shining', 'tall',
    'thundering',
    'tenacious', 'twilight', 'wandering', 'weathered', 'white', 'wild', 'winter',
    'wispy',
    'withered', 'yeadon'
]

nouns = [
    'art', 'band', 'bar', 'base', 'brick', 'block', 'boat', 'brake',
    'wall', 'breeze', 'brook', 'car', 'speaker', 'cone', 'cell', 'calendar',
    'cloud', 'credit', 'darkness', 'dawn', 'dam', 'disk', 'dream', 'dust',
    'feather', 'field', 'fire', 'firefly', 'factory', 'fog', 'forest', 'note',
    'frost', 'glade', 'glitter', 'grass', 'hall', 'hat', 'haze', 'home',
    'hill', 'king', 'lab', 'lake', 'leaf', 'limit', 'math', 'meadow',
    'mode', 'moon', 'morning', 'mountain', 'mist', 'mud', 'night', 'paper',
    'pine', 'poetry', 'pond', 'quilt', 'rain', 'recipe', 'resonance', 'rice',
    'river', 'sail', 'scene', 'sea', 'shadow', 'shape', 'silence', 'sky',
    'smoke', 'snow', 'snowflake', 'sound', 'star', 'sun', 'sun', 'sunset',
    'surf', 'term', 'thunder', 'tea', 'travel', 'time', 'union', 'string',
    'violet', 'voice', 'water', 'waterfall', 'wave', 'wildflower', 'wind',
    'water'
]

